// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePawn.h"
#include "Tank.generated.h"

/**
 * 
 */
UCLASS()
class TOONTANKS_API ATank : public ABasePawn
{
	GENERATED_BODY()
	
public:
	// Constructor
	ATank();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
private:
	UPROPERTY(VisibleAnywhere, Category="Components")
	class USpringArmComponent* TankSpringArm = nullptr;
	
	UPROPERTY(VisibleAnywhere, Category="Components")
	class UCameraComponent* SpringArmCamera = nullptr;

	UPROPERTY(EditAnywhere, Category="Tank Movement")
	float TankMoveSpeed = 1.f;

	UPROPERTY(EditAnywhere, Category="Tank Movement")
	float TankTurnSpeed = 1.f;

	UPROPERTY(EditAnywhere, Category="Tank Movement")
	float TurretRotationSpeed = 1.f;
	
	void Move(float inputValue);
	void Turn(float inputValue);
	void TurretRotate(float inputValue);
	float ReturnWithDelta(const float& inputValue) const;

	UPROPERTY()
	APlayerController* PlayerControllerRef = nullptr;
};