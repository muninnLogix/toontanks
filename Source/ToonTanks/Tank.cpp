// Fill out your copyright notice in the Description page of Project Settings.


#include "Tank.h"
#include "Camera/CameraComponent.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"

ATank::ATank()
{
	// Create springarm and attach to CapsuleComp
	TankSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Tank SpringArm"));
	SpringArmCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Tank SpringArm Camera"));
	SpringArmCamera->SetupAttachment(TankSpringArm);

	if ( !TurretMesh )
	{
		UE_LOG(LogTemp, Error, TEXT("Actor %s does not have component TurretMesh!"), *GetName());
		return;
	}
	
	TankSpringArm->SetupAttachment(TurretMesh);
}

void ATank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATank::Move);
	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &ATank::Turn);
	PlayerInputComponent->BindAxis(TEXT("RotateTurret"), this, &ATank::TurretRotate);
}

void ATank::BeginPlay()
{
	Super::BeginPlay();

	PlayerControllerRef = Cast<APlayerController>(GetController());
	if ( !PlayerControllerRef ) { UE_LOG(LogTemp, Error, TEXT("NO PLAYERCONTROLLERREF")) return;}

}


// Called every frame
void ATank::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if ( PlayerControllerRef )
	{
		FHitResult mouseCursorHit;
		PlayerControllerRef->GetHitResultUnderCursor(
			ECC_Visibility,
			false,
			mouseCursorHit
		);

		DrawDebugSphere(
			GetWorld(),
			mouseCursorHit.Location,
			10.f,
			6,
			FColor().Red
		);
	}
}


void ATank::Move(float inputValue)
{
	const FVector tankMovementVector = {
		TankMoveSpeed * ReturnWithDelta(inputValue),
		0.f,
		0.f
	};
	AddActorLocalOffset(tankMovementVector, true);
}

void ATank::Turn(float inputValue)
{
	const FRotator tankRotator = {
		0.f,
		TankTurnSpeed * ReturnWithDelta(inputValue),
		0.f
	};
	AddActorLocalRotation(tankRotator, true);
}

void ATank::TurretRotate(float inputValue) 
{
	const FRotator turretRotator = {
		0.f,
		TurretRotationSpeed * ReturnWithDelta(inputValue),
		0.f
	};

	
	if ( !TurretMesh )
	{
		UE_LOG(LogTemp, Error, TEXT("Actor %s does not have component TurretMesh!"), *GetName());
		return;
	}
	
	TurretMesh->AddLocalRotation(turretRotator, true);
}

float ATank::ReturnWithDelta(const float& inputValue) const
{
	const float deltaTime = UGameplayStatics::GetWorldDeltaSeconds(this);
	return inputValue * deltaTime;
}